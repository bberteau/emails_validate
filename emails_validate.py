from browser import document
from browser import alert
import re
import time
import string

def is_valid_email(email):
    # http://sametmax.com/valider-une-adresse-email-avec-une-regex-en-python/
    email_re = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"' # quoted-string
        r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)  # domain
    return email_re.search(email)

@document['process_button'].bind('click')
def validate_emails(ev):
    time_start = time.time()
    print("C'est parti !")
    # caratères à supprimer dans la liste
    caracteres = [";", ",", " ", "/", "\t", "\r", "\\","\"", "<", ">", "(", ")", ":", "|","^K"]
    # Suppression du contenu de mails_output
    document['mails_output'].text = ""
    # On récupère le texte du bouton
    process_text = document['process_button'].text
    # On désactive l'événement "click" du bouton
    document['process_button'].unbind("click", validate_emails)
    # Changement du texte du bouton pour montrer que le processus est lancé
    document['process_button'].text = "En cours..."
    document['process_button'].classList.add("btn_disabled")
    mails_list = document["mails_input"].value
    mails_list = mails_list.lower()
    for car in caracteres:
        mails_list = mails_list.replace(car, '\n')
    mails_list = ''.join([x if x in string.printable else '\n' for x in mails_list])
    # mails_list = filter(lambda x: x in string.printable, mails_list)
    mails_list = list(set(mails_list.split('\n'))) # dédoublonnage
    # mails_list = list(mails_list)
    mails_list = sorted(mails_list)
    mails_output = [ mail.strip() for mail in mails_list if "@" in mail and is_valid_email(mail)]
    print("Emails valides : {}".format(len(mails_output)))
    document['mails_output'].text = "\n".join(mails_output)
    document['process_button'].text = process_text
    print("Durée du traitement : {:.2f} secondes".format(time.time()-time_start))
    # Réactivation du "clic" sur le bouton
    document['process_button'].bind("click", validate_emails)
    document['process_button'].classList.remove("btn_disabled")
    return

